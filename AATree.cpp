//
//  AATree.cpp
//  Michael Corso

#include "AATree.h"
#include "Flags.h"

/* **************************************************************** */

#if CONSTRUCTOR || ALL
template <class T>
AATree<T>::AATree()
{
  bottomNode = new Node<T>();
  bottomNode->level = 0;
  bottomNode->left = bottomNode;
  bottomNode->right = bottomNode;
  bottomNode->data = -1;
  
  root = bottomNode;
  lastNode = bottomNode;
  deletedNode = bottomNode;
}
#endif

/* **************************************************************** */

#if DESTRUCTOR || ALL
template <class T>
AATree<T>::~AATree()
{
  makeEmpty();
  delete bottomNode;
}
#endif

/* **************************************************************** */

#if ISEMPTY || ALL
template <class T>
bool AATree<T>::isEmpty(){
  if(root == bottomNode){
    return true;
  }
  return false;
}
#endif

/* **************************************************************** */

#if FIND || ALL
template <class T>
bool AATree<T>::find(const T & x){
  if( findNode(root, x) == bottomNode ){
    return false;
  }else if( findNode(root, x)->data == x ){
    return true;
  }else{
    return false;
  }
}
#endif

/* **************************************************************** */

#if FINDNODE || ALL
template <class T>
Node<T> * AATree<T>::findNode(Node<T> * node, const T & x){
  Node<T> * n = node;
  while(n != bottomNode && n->data != x){
    if( x < n->data ){
      n = n->left;
    }else{
      n = n->right;
    }
  }
  return n;
}
#endif

/* **************************************************************** */

#if FINDMIN || ALL
template <class T>
const T & AATree<T>::findMin(){
  return findMinNode(root)->data;
}
#endif

/* **************************************************************** */

#if FINDMINNODE || ALL
template <class T>
Node<T> * AATree<T>::findMinNode(Node<T> * node){
  Node<T> * n = node;
  if(n == bottomNode){
    return bottomNode;
  }
  if(n != bottomNode){
    while( n->left != bottomNode ){
      n = n->left;
    }
  }
  return n;
}

#endif

/* **************************************************************** */

#if FINDMAX || ALL
template <class T>
const T & AATree<T>::findMax(){
  return findMaxNode(root)->data;
}
#endif

/* **************************************************************** */

#if FINDMAXNODE || ALL
template <class T>
Node<T> * AATree<T>::findMaxNode(Node<T> * node){
  Node<T> * n = node;
  if(n != bottomNode){
    while( n->right != bottomNode ){
      n = n->right;
    }
  }
  return n;
}
#endif

/* **************************************************************** */

#if INSERT || ALL
template <class T>
void AATree<T>::insert(const T & x){
  root = insertNode(root, x);
}
#endif

/* **************************************************************** */

#if INSERTNODE || ALL
template <class T>
Node<T> * AATree<T>::insertNode(Node<T> * & node, const T & x){
  if(node == bottomNode){
    node = new Node<T>(x, bottomNode, bottomNode, 1);
  }else{
    if(x < node->data){
      insertNode(node->left, x);
    }else if(x > node->data){
      insertNode(node->right, x);
    }else{
      //Duplicate item
      //nothing to do
      return bottomNode;
    }
    skew(node);
    split(node);	
  }
  return node;
}
#endif

/* **************************************************************** */

#if REMOVE || ALL
template <class T>
void AATree<T>::remove(const T & x){
  removeNode(root, x);
}
#endif

/* **************************************************************** */

#if REMOVENODE || ALL
template <class T>
void AATree<T>::removeNode(Node<T> * & node, const T & x){
  if(node != bottomNode){
    // 1: search down the tree and set pointers
    // for the lastNode and the deletedNode
    lastNode = node;
    if(x < node->data){
      removeNode(node->left, x);
    }else{
      deletedNode = node;
      removeNode(node->right, x);
    }
  }
  // 2: at the bottom of the tree we
  // remove the element (if it is present)
  if( (node == lastNode) && (deletedNode != bottomNode) && (x == deletedNode->data) ){
    deletedNode->data = node->data;
    deletedNode = bottomNode;
    node = node->right;
    delete lastNode;
  }
  // 3: on the way back, we rebalance
  //
  else if( (node->left->level < node->level - 1) || (node->right->level < node->level - 1) ){
    node->level = node->level - 1;
    if(node->right->level > node->level){
      node->right->level = node->level;
    }
    skew(node);
    skew(node->right);
    skew(node->right->right);
    split(node);
    split(node->right);
  }
}
#endif

/* **************************************************************** */

#if SKEW || ALL
template <class T>
void AATree<T>::skew(Node<T> * & node){
  //from original paper
  if(node->left->level == node->level){
    Node<T> * temp = node;
    node = node->left;
    temp->left = node->right;
    node->right = temp;
  }
}
#endif

/* **************************************************************** */

#if SPLIT || ALL
template <class T>
void AATree<T>::split(Node<T> * & node){
  //from original paper
  if(node->right->right->level == node->level){
    Node<T> * temp = node;
    node = node->right;
    temp->right = node->left;
    node->left = temp;
    node->level++;
  }
}
#endif

/* **************************************************************** */

#if MAKEEMPTY || ALL
template <class T>
void AATree<T>::makeEmpty(){
  removeAllNodes(root);
  root = bottomNode;
}
#endif

/* **************************************************************** */

#if REMOVEALLNODES || ALL
template <class T>
void AATree<T>::removeAllNodes(Node<T> * node){
  if(node != bottomNode){
    removeAllNodes(node->left);
    removeAllNodes(node->right);
    delete node;
  }
}
#endif

/* **************************************************************** */



/* **************************************************************** */
/* Do NOT modify anything below this line                           */
/* **************************************************************** */

#ifndef BUILD_LIB
// Print tree
template <class T>
void AATree<T>::printTree(){
    if (root != bottomNode)
    {
        printNodesInOrder(root);
        std::cout << std::endl;
    } else {
        std::cout << "Empty Tree" << std::endl;
        std::cout << std::endl;
    }
}

// Print tree using level order traversal
template <class T>
void AATree<T>::printNodesInOrder(Node<T> * node)
{
    Node<T>* q[100];
    int head = 0;
    int tail = 0;
    q[0] = root;
    tail++;

    while (head != tail)
    {
        Node<T> *n = q[head];
        head++;
        std::cout << "Node " << n->data << " at level " << n->level << std::endl;
        if (n->left != bottomNode)
        {
            std::cout << "  " << n->data << " left child: " << n->left->data << std::endl;
            q[tail] = n->left;
            tail++;
        }
        if (n->right != bottomNode)
        {
            std::cout << "  " << n->data << " right child: " << n->right->data << std::endl;
            q[tail] = n->right;
            tail++;
        }
    }
}
#endif

template class AATree<int>;
